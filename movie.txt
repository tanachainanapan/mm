{
"name": "หนัง",
"author": "issac",
"image": "https://image.freepik.com/free-vector/movie-cinema-premiere-background_41737-251.jpg",
"url": "https://gitlab.com/-/ide/project/zzxc/issac/tree/main/-/movie.txt/",
"stations": [
    {
"name": "mmmmจักรกลเลือดดุ - Bloodshot",  
"image": "https://cdn-img.movieclub.tv/img_movies_vertical/1584340654_bloodshot_cover.jpg",
"url": "https://cache-movie.nungfree.tv/hls/content/nas06/mvc/M/Inter/Reminiscence_2021/T.mp4/index.m3u8?st=yb_DjFgXEhCEuC9QXDCpgw&e=1634507400&u=nungfree",
"playInNatPlayer": "true"
},
			{
"name": "จักรกลเลือดดุ - Bloodshot",  
"image": "https://cdn-img.movieclub.tv/img_movies_vertical/1584340654_bloodshot_cover.jpg",
"url": "https://cache-edge-app2.movieclubhd.tv/hls/content/nas05/mvc/M/Inter/Bloodshot-2020/Bloodshot-TH51.mp4/index.m3u8",
"playInNatPlayer": "true"
},
{
"name": "ยึดแผ่นดิน เปลี่ยนพันธุ์มนุษย์ - District 9 (2009)",  
"image": "https://www.nungfree.tv/images/movie/0d38d698cc8676e759db210ab32f7e08.webp",
"url": "https://cache-edge-app2.movieclubhd.tv/hls/content/nas05/mvc/M/Inter/District_9_2009/District_9_2009_TH.mp4/index.m3u8",
"playInNatPlayer": "true"
},
{
"name": "แพนดอรัม ลอกชีพ - Pandorum",  
"image": "https://www.nungfree.tv/images/movie/44b85868e673c77153ce173bddc6bd47.webp",
"url": "https://cache-edge-app2.movieclubhd.tv/hls/content/nas01-1/mvc/m/inter/Pandorum/Pandorum_T.mp4/index.m3u8",
"playInNatPlayer": "true"
},
{
"name": "ซูเปอร์นักรบดับทัพอสูร - Edge of Tomorrow (2014)",  
"image": "https://www.nungfree.tv/images/movie/fea7dd9951ee28b4e2bd24880b60d67c.webp",
"url": "https://cache-edge-app2.movieclubhd.tv/hls/content/nas05/mvc/M/Inter/Edge_Of_Tomorrow_2014/Edge_Of_Tomorrow_2014_TH.mp4/index.m3u8",
"playInNatPlayer": "true"
},
{
"name": "คนโคม่า วิ่ง คลั่ง ฆ่า 4K - Crank 4K",  
"image": "https://www.nungfree.tv/images/movie/0feed5267743706f29e11879a04622f8.webp",
"url": "https://cache-edge-app2.movieclubhd.tv/hls/content/nas04/mvc/m/4k/Crank/Crank_T.mp4/index.m3u8",
"playInNatPlayer": "true"
},
{
"name": "Black Hawk Down ยุทธการฝ่ารหัสทมิฬ (2001) - Black Hawk Down ยุทธการฝ่ารหัสทมิฬ (2001)",  
"image": "https://cdn-img.movieclub.tv/img_movies_vertical/1337.jpg",
"url": "https://cache-edge-app2.movieclubhd.tv/hls/content/nas01/movie/eng/chunklist_wmeng3701.MP4/index.m3u8",
"playInNatPlayer": "true"
},
{
"name": "ศึกหุ่นเหล็กกำปั้นถล่มปฐพี - Real Steel (2011)",  
"image": "https://www.nungfree.tv/images/movie/4088445ab616d7aa0dd375ced77801a5.webp",
"url": "https://cache-edge-app2.movieclubhd.tv/hls/content/nas03/mvc/m/inter/Real_Steel_2011/Real_Steel_2011_TH.mp4/index.m3u8",
"playInNatPlayer": "true"
},
		{
"name": "A.I. Artificial Intelligence จักรกลอัจฉริยะ - A.I. Artificial Intelligence จักรกลอัจฉริยะ",  
"image": "https://www.nungfree.tv/images/movie/f85d38e9b95cc1f3b937adb068241c2a.webp",
"url": "https://cache-edge-app2.movieclubhd.tv/hls/content/nas01-1/mvc/m/inter/A_I_Artificial_Intelligence_2001/Artificial_Intelligence_2001_ST.mp4/index.m3u8",
"playInNatPlayer": "true"
},
	{
"name": "พลิกชะตา เร็วกว่านรก - Replicas",  
"image": "https://cdn-img.movieclub.tv/img_movies_vertical/1549942001_replicas_cover.jpg",
"url": "https://cache-edge-app2.movieclubhd.tv/hls/content/nas01-1/mvc/m/inter/Replicas/Replicas_T.mp4/index.m3u8",
"playInNatPlayer": "true"
},
	{
"name": "Interstellar (2014)",  
"image": "https://upload.wikimedia.org/wikipedia/en/b/bc/Interstellar_film_poster.jpg",
"url": "https://cache-edge-app2.movieclubhd.tv/hls/content/nas05/mvc/M/Inter/Interstellar_2014/Interstellar_2014_TH.mp4/index.m3u8",
"playInNatPlayer": "true"
},
	{
"name": "โรงแรมโคตรมหาโจร - Hotel Artemis",  
"image": "https://www.nungfree.tv/images/movie/624786f5adf563b1f0a689f2859f0641.webp",  
"url": "https://cache-edge-app2.movieclubhd.tv/hls/content/nas03/mvc/m/inter/Hotel-Artemis-Z/Hotel-Artemis-Z-1.mp4/index.m3u8",
"playInNatPlayer": "true"
},
		{
"name": "ดิ โอลด์ การ์ด - The Old Guard (2020)",  
"image": "https://www.nungfree.tv/images/movie/cf15ed9dc93f9c9f7d90e098c042ab78.webp",  
"url": "https://cache-edge-app2.movieclubhd.tv/hls/content/nas05/mvc/M/Inter/The-Old-Guard/The-Old-Guard-T.mp4/index.m3u8",
"playInNatPlayer": "true"
},
	{
"name": "โลแกน เดอะ วูล์ฟเวอรีน - Logan
เมื่อในอนาคตเหล่ามนุษย์กลายพันธุ์เข้าสู่ยุคล่ม",  
"image": "https://www.nungfree.tv/images/movie/6f8ef77370071d3886375e3ae1f111d8.webp",  
"url": "https://cache-edge-app2.movieclubhd.tv/hls/content/nas03/mvc/m/inter/Logan/Logan_T.mp4/index.m3u8",
"playInNatPlayer": "true"
},
	{
"name": "ลอร์ดออฟเดอะริงส์ อภินิหารแหวนครองพิภพ ภาค 1 - The Lord of the Rings 1 The Fellowship of the Ring",  
"image": "https://cdn-img.movieclub.tv/img_movies_vertical/6e43a0da4fbb7788a8f3455f07c7f94c.jpg",  
"url": "https://cache-edge-app2.movieclubhd.tv/hls/content/nas03/mvc/m/inter/The-Lord-Of-The-Rings-1-3/The-Lord-Of-The-Rings_T.mp4/index.m3u8",
"playInNatPlayer": "true"
},
	{
"name": "Arrival ผู้มาเยือน (2016) - Arrival ผู้มาเยือน (2016)",  
"image": "https://www.nungfree.tv/images/movie/9fb6d5def6155e046376cfcfccaf2f33.webp",  
"url": "https://cache-edge-app2.movieclubhd.tv/hls/content/nas01/movie/eng/chunklist_wmeng0615.MP4/index.m3u8",
"playInNatPlayer": "true"
},
	{
"name": "Kingsman The Secret Service",  
"image": "https://www.nungfree.tv/images/movie/ccf599b459703089ab267463af7e4076.webp",  
"url": "https://cache-edge-app2.movieclubhd.tv/hls/content/nas05/mvc/M/Inter/Kingsman-the-secret-service/Kingsman-the-secret-service_T.mp4/index.m3u8?st=TX8Y9jxo82Hbd9bQnmUfig&e=1632760667&u=nungfree?",
"playInNatPlayer": "true"
},
{
"name": "คิงส์แมน รวมพลังโคตรพยัคฆ์ - Kingsman The Golden Circle",  
"image": "https://www.nungfree.tv/images/movie/58ae8c4ed234605da77038a939e54d3d.webp",  
"url": "https://cache-edge-app2.movieclubhd.tv/hls/content/nas03/mvc/m/inter/Kingsman-the-golden-circle/Kingsman-the-golden-circle_T.mp4/index.m3u8?",
"playInNatPlayer": "true"
},
{
"name": "Baby Driver (2017)",  
"image": "https://www.nungfree.tv/images/movie/2abc1a54082908bb20822ff1f0221624.webp",  
"url": "https://cache-edge-app2.movieclubhd.tv/hls/content/nas03/mvc/m/inter/baby-driver/baby-driver_T.mp4/index.m3u8",
"playInNatPlayer": "true"
},
{
"name": "คู่สืบคู่แสบ - Central Intelligence (2016)",  
"image": "https://www.nungfree.tv/images/movie/41adfee9203c87066c375a5696ad30e0.webp",  
"url": "https://cache-edge-app2.movieclubhd.tv/hls/content/nas05/mvc/M/Inter/Central_Intelligence_Theatrical_2016/Central_Intelligence_Theatrical_2016_TH.mp4/index.m3u8",
"playInNatPlayer": "true"
},
{
"name": "ฆาตกรรมหรรษา ใครฆ่าคุณปู่ - Knives Out (2019)",  
"image": "https://www.nungfree.tv/images/movie/d8530a668c5bbeea9dd189b5df8548ea.webp",
"url": "https://cache-edge-app2.movieclubhd.tv/hls/content/nas03/mvc/m/inter/Knives_Out_2019/Knives_Out_2019_TH.mp4/index.m3u8",
"playInNatPlayer": "true"
},
{
"name": "Hard Kill (2020) ไล่ล่าฆ่าไม่ตาย",  
"image": "https://www.nanamovies.me/wp-content/uploads/2021/05/Hard-Kill-2020-ไล่ล่าฆ่าไม่ตาย-Full-HD.jpeg",
"url": "https://ok.ru/video/2433411058340",
"playInNatPlayer": "true"
},
{
"name": "Godzilla vs. Kong (2021) ก็อดซิลล่า ปะทะ คอง",  
"image": "https://www.nanamovies.me/wp-content/uploads/2021/03/Godzilla-vs.-Kong-2021-ก็อดซิลล่า-ปะทะ-คอง-ซูม.jpeg",
"url": "https://www458.ff-01.com/token=X3_OQfQLOETcAlKuaCpNyg/1621461799/223.207.0.0/168/a/3e/481c7033caaaa43c8cdb6287d9ab23ea-1080p.mp4",
"playInNatPlayer": "true"
},
{
"name": "Zack Snyder’s Justice League (2021)",  
"image": "https://www.i-moviehd.com/wp-content/uploads/2021/03/Zack-Snyders-Justice-League-2021-1.png",
"url": "https://we-play.tv/watching/movie/zack-snyders-justice-league-2021",
"playInNatPlayer": "true"
},
{
"name": "I Am a Hero (2016) ข้าคือฮีโร่",  
"image": "https://www.movie-24th.com/wp-content/uploads/2020/05/53c8010a17bdbbe337a07841f4e99218.webp",
"url": "https://ok.ru/video/2364039301796",
"playInNatPlayer": "true"
},
{
"name": "1917",  
"image": "https://static.posttoday.com/media/content/2020/01/23/D06098C60927442D9B31D44015A6A328.jpg",
"url": "https://we-play.tv/watching/movie/1917-2019",
"playInNatPlayer": "true"
},
{
"name": "ALITA : BATTLE ANGEL",  
"image": "http://www.urbanbox.in.th/wp-content/uploads/2019/02/alita-battle-angel-960x600.jpg",
"url": "https://we-play.tv/watching/movie/alita-battle-angel-2019",
"playInNatPlayer": "true"
},
{
"name": "Greyhound (2020) บรรยายไทย",  
"image": "https://www.flashfly.net/wp/wp-content/uploads/2020/07/46376bd0-ac80-11ea-b279-da437c8b8f74-1140x570.jpg",
"url": "https://we-play.tv/watching/movie/greyhound-2020",
"playInNatPlayer": "true"
},
{
"name": "Birds of Prey ทีมนกผู้ล่า กับฮาร์ลีย์ ควินน์",  
"image": "https://s359.kapook.com/pagebuilder/72d9ac5e-c308-4b94-88d7-4732dd9a14fb.jpg",
"url": "https://we-play.tv/watching/movie/birds-of-prey-and-the-fantabulous-emancipation-of-one-harley-quinn-2020",
"playInNatPlayer": "true"
},
{
"name": "Attack of the Unknown (2020)",  
"image": "https://www.nanamovies.me/wp-content/uploads/2021/04/Attack-of-the-Unknown-2020-Full-HD.jpeg",
"url": "https://ok.ru/video/2386068703908",
"playInNatPlayer": "true"
},
{
"name": "The Invisible Man (2020)",  
"image": "https://gdb.voanews.com/F1E1541B-010E-4C3D-AB74-29DC2BE07BE0_w1597_n_r0_st.jpg",
"url": "https://we-play.tv/watching/movie/the-invisible-man-2020",
"playInNatPlayer": "true"
},
{
"name": "Sonic the Hedgehog (2020)",  
"image": "https://s.isanook.com/ga/0/rp/r/w728/ya0xa0m1w0/aHR0cHM6Ly9zLmlzYW5vb2suY29tL2dhLzAvdWQvMjEwLzEwNTQzNjkvc29uaWMtdGhlLWhlZGdlaG9nLmpwZw==.jpg",
"url": "https://we-play.tv/watching/movie/sonic-the-hedgehog-2020",
"playInNatPlayer": "true"
},
{
"name": "THE CALL OF THE WILD เสียงเพรียกจากพงไพร (2020)",  
"image": "https://www.khaosod.co.th/wpapp/uploads/2020/03/H4A.jpg",
"url": "https://we-play.tv/watching/movie/the-call-of-the-wild-2020",
"playInNatPlayer": "true"
},
{
"name": "Now You See Me อาชญากลปล้นโลก (2013)",  
"image": "https://resizing.flixster.com/Spc60Xz-JLU8_Xxlk4-o39FFddE=/206x305/v2/https://flxt.tmsimg.com/assets/p9418112_p_v10_aq.jpg",
"url": "https://we-play.tv/watching/movie/now-you-see-me-2013",
"playInNatPlayer": "true"
},
{
"name": "Now You See Me 2 อาชญากลปล้นโลก (2016)",  
"image": "https://upload.wikimedia.org/wikipedia/en/9/9a/Now_You_See_Me_2_poster.jpg",
"url": "https://we-play.tv/watching/movie/now-you-see-me-2-2016",
"playInNatPlayer": "true"
},
{
"name": "Crawl คลานขย้ำ (2019)",  
"image": "https://f.ptcdn.info/509/065/000/pw9ytkfnkUQq2K8G4JO-o.jpg",
"url": "https://we-play.tv/watching/movie/crawl-2019",
"playInNatPlayer": "true"
},
{
"name": "Rambo: Last Blood แรมโบ้ 5 นักรบคนสุดท้าย (2019)",  
"image": "https://s359.kapook.com/pagebuilder/eda030bf-d8df-4784-9f9e-d7b7d748d43e.jpg",
"url": "https://we-play.tv/watching/movie/rambo-last-blood-2019",
"playInNatPlayer": "true"
},
{
"name": "Vanguard แวนการ์ด หน่วยพิทักษ์ฟัดข้ามโลก (2020)",  
"image": "https://www.beartai.com/wp-content/uploads/2020/10/vanguard-poster-712x1024.jpg",
"url": "https://we-play.tv/watching/movie/vanguard-2020",
"playInNatPlayer": "true"
},
{
"name": "Stand by Me Doraemon (2014) โดราเอมอน เพื่อนกันตลอดไป",  
"image": "https://www.037hdmovie.com/wp-content/uploads/2018/04/yGLijiDmNO6azCtPqLq.jpg",
"url": "https://www24.sbvideocdn.com/hls/tysxen7l5k66j6cdaczrrr2bojde62jxfyhk6au4ezmlirzkej4hjccmoita/index-v1-a1.m3u8",
"playInNatPlayer": "true"
},
{
"name": "Gantz (2010)",  
"image": "https://www.nanamovies.me/wp-content/uploads/2017/10/Gantz-2010.jpeg",
"url": "https://www484.ff-01.com/token=Oo8gj6nvEDOVy-FodjXWiQ/1621462565/223.207.0.0/163/6/2b/58afc6a7674f4c2587487a6d209f82b6-1080p.mp4",
"playInNatPlayer": "true"
},
{
"name": "Gantz 2: Perfect Answer (2011)",  
"image": "https://www.nanamovies.me/wp-content/uploads/2017/10/Gantz-2-Perfect-Answer-2011.jpeg",
"url": "https://ok.ru/video/2411408788132",
"referer": "https://www.nanamovies.me",
"playInNatPlayer": "true"
}

	]
}
